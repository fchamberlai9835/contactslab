import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactsSorter extends Contact {
    static ArrayList<Contact> ContactsList = new ArrayList<>();
    static ArrayList<Contact> FinalContactsList = new ArrayList<>();

    public static void main(String[] args) {
        runProgram();
    }

    static void runProgram() {
        System.out.print("Hello and Welcome to Contact Sorting! Please enter file name for contacts: ");
        Scanner system = new Scanner(System.in);
        SetContactsList(system.nextLine());
        System.out.println("\nHere is your contact list: ");
        for (int i = 0; i < ContactsList.size(); i++) {
            System.out.println(ContactsList.get(i).getName() + " " + ContactsList.get(i).getAddress());
        }
        System.out.print("How would you like to sort? ");
        String next = system.nextLine();
        int[] arr = new int[ContactsList.size()];
        for (int i = 0; i < ContactsList.size(); i++) {
            arr[i] = ContactsList.get(i).getAge();
        }
        if (next.equalsIgnoreCase("bogo")) {
            bogoSort(arr);
        } else if (next.equalsIgnoreCase("bubble")) {
            bubbleSort(arr);
        } else if (next.equalsIgnoreCase("merge")) {
            sort(arr, 0, arr.length - 1);
        }
        for (int i = 0; i < arr.length; i++) {
            int j = 0;
            while (j < ContactsList.size()) {
                if (ContactsList.get(j).getAge() == arr[i]) {
                    FinalContactsList.add(ContactsList.get(i));
                }
                j++;
            }
        }
        System.out.println("Here is your sorted Contacts List: ");
        for (int i = 0; i < ContactsList.size(); i++) {
            System.out.println(ContactsList.get(i).getName() + " " + ContactsList.get(i).getAddress());
        }
    }

    static void SetContactsList(String fileName) {
        Pattern studentPattern = Pattern.compile("^(?<firstName>[A-Za-z]+)\\s(?<lastName>[A-Za-z]+)\\s+(?<age>\\d+)\\s+(?<address>\\d+)$");
        try {
            FileReader fin = new FileReader(fileName);
            Scanner src = new Scanner(fin);
            String activeLine;
            Matcher studentMatcher;
            src.nextLine();
            while (src.hasNext()) {
                activeLine = src.nextLine();
                studentMatcher = studentPattern.matcher(activeLine);
                studentMatcher.find();
                Contact contact = new Contact();
                contact.setName(studentMatcher.group("firstName") + studentMatcher.group("lastName"));
                contact.setAge(Integer.parseInt(studentMatcher.group("age")));
                contact.setAddress(studentMatcher.group("address"));
                ContactsList.add(contact);
            }
        } catch (Exception e) {
            System.out.println(e);
            runProgram();
        }
    }

    static void bubbleSort(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    static void bogoSort(int[] a) {
        while (isSorted(a) == false) {
            shuffle(a);
        }
    }

    static void shuffle(int[] a) {
        int n = a.length - 1;
        for (int i = 1; i <= n; i++) {
            swap(a, i, (int) (Math.random() * i));
        }
    }

    static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    static boolean isSorted(int[] a) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            if (a[i] < a[i - 1]) {
                return false;
            }
        }
        return true;
    }

    static void merge(int arr[], int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        int L[] = new int[n1];
        int R[] = new int[n2];

        for (int i = 0; i < n1; i++) {
            L[i] = arr[l + i];
        }
        for (int j = 0; j < n2; j++) {
            R[j] = arr[m + 1 + j];
        }
        int i = 0;
        int j = 0;
        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    static void sort(int arr[], int l, int r) {
        if (l < r) {
            int m = l + (r - l) / 2;
            sort(arr, l, m);
            sort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
    }
}
