public class Contact extends Person {
    private String address;

    public int getAge() {
        return super.getAge();
    }

    public String getName() {
        return super.getName();
    }

    public void setAge(int age) {
        super.setAge(age);
    }

    public void setName(String name) {
        super.setName(name);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return "Contact{" + "Address = " + address + '}';
    }
}
